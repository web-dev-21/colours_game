# colours_game

# README for Michelle and Ivana's Colours Game

## File Structure

- `index.html`: The main HTML document that loads the game interface.
- `css/`: Contains three CSS files for styling:
- `layout.css`: General layout styles.
- `game.css`: Specific styles for the game components.
- `media.css`: Media queries for responsive design.
- `script/`: Contains five JavaScript files with the game's functionality:
- `board.js`: Handles the display and dynamic generation of the game board.
- `cheatMode.js`: Enables a cheat mode to see the predominant color on the tiles.
- `highscore.js`: Manages the high score table and local storage.
- `form.js`: Manages form interactions and validates user input.
- `gameMessageFunctions.js`: Provides messages and feedback to the player about the game state.

## Setup

- Launching the Game: Open the `index.html` file in a web browser to start the game.

## How to Play

1. Enter player name (alphanumeric, at least 5 characters long).
2. Choose the game board size (between 3x3 to 7x7).
3. Select a color to find on the game board.
4. Adjust the difficulty level (the higher, the less color variance).
5. Click "Start Game!" to begin.
6. Select the tiles that match the target color.
7. Submit your guess by pressing "Submit your guess" to see if you've chosen the correct tiles.
8. You can keep track of how many tiles you need to select and how many you've selected with the message below the board.

## High Scores

- Scores are saved in the browser's local storage.
- You can view the high scores on the high score board 
- You can toggle between ascending and descending order by clicking on the "High Scores" header.
- You can reset the high score table, click the "Clear Highscore" button.

## Additional Features

- Cheat mode can be toggled by pressing Shift + C, showing the dominant color of the tiles.

## Credits

- Game designed and programmed by Ivana Zhekova & Uyen Dinh Michelle Banh.
- Inspiration for some features from Vijay Patel and received the getScore() from Samuel Imberman.
- Copyright &copy; Ivana Zhekova & Uyen Dinh Michelle Banh, 5th of November, 2023.

