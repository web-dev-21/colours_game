"use strict";

/**
 * @author Ivana Zhekova & Uyen Dinh Michelle Banh
 * 
 * board.js is used to display our game.
 * it will generate the table when we start the game,
 * it will show a message to the user with what
 * the target color is, how many they must select (target) 
 * and how many the player has selected.
 * 
 */

/**
* @author Vijay Patel - got inspiration from him
* copyrighted
*
* this will change the title of the page to come back because it's funny
*/

document.addEventListener("visibilitychange", function(){
    let title = document.querySelector("title");
   
    if(document.visibilityState !== 'visible'){
      title.textContent = "😭 come back <3";
    } else {
      title.textContent = "Game!!!";
    }

});

document.addEventListener("DOMContentLoaded", function () {

    resetForm();
    setLocalStorage();

    /**
    * 
    * @description when clicking the start button, 
    *              it will generate the game board and the message to play the game.
    *              Afterwards, the controls in the Game Setup will be disabled
    *              and the submit guess button will be enabled. 
    */
    const startButton = document.getElementById("gameStart");

    startButton.addEventListener("click", function startGame(e) {
        e.preventDefault();

        const userColor = document.getElementById("color").value;
        const form = document.querySelector("#setup form");
        const boardGame = document.getElementById("board");

        //Before generating a table, it check of the form is valid
        if (form.checkValidity()) {
            let target;
            do {
                generateTable();
                target = getTargetNumber();
            } while (target === 0);

            const guessButton = createButtonElement('Submit Your Guess!', 'guessButton');
            const message = createMessageElement(`Searching for ${userColor}! Your target is ${target}!`);
            
            guessButton.disabled = false;
            guessButton.addEventListener("click", function () {
                enableFormElements(form);
                completionPercentage();
                resetForm();
            });
            
            boardGame.appendChild(message);
            boardGame.appendChild(guessButton);
            

            disableFormElements(form);
            startButton.disabled = true;
        }
    });

     /**
     * @function generateTable
     * @description Generates the table layout for the game board
     *              using the function createTableElement
     */

    function generateTable() {
        const boardGame = document.getElementById("board");
        const size = document.getElementById('size').value;
        const difficulty = document.getElementById('difficulty').value;
        boardGame.textContent = '';

        const table = createTableElement(size, difficulty);
        boardGame.appendChild(table);
    }

    /**
    * @function createTableElement
    * @description Creates a table element with specified size and difficulty settings.
    * @param {number} size - The number of cells in one row or column of the table.
    * @param {string} difficulty - The difficulty level that influences the color of the cells.
    * @returns The table element.
    */

    function createTableElement(size, difficulty) {
        const table = document.createElement("table");
        table.setAttribute("id", "boardArea");
        table.classList.add("gameTable");

        for (let addRows = 0; addRows < size; addRows++) {
            const row = document.createElement("tr");
            table.appendChild(row);

            for (let addCols = 0; addCols < size; addCols++) {
                const cell = document.createElement("td");
                row.appendChild(cell);
                cell.style.backgroundColor = boardColorChange(difficulty);

                cell.addEventListener("click", function () {
                    if (cell.classList.contains("selected")) {
                        cell.classList.remove("selected");
                    } else {
                        cell.classList.add("selected");
                    }
                });
                cell.addEventListener("click", getSelectedTiles);
            }
        }
        return table;
    }

    /**
    * @function  createButtonElement
    * @description Creates a button element with specified text and ID.
    * @param {string} buttonText - The text to display on the button.
    * @param {string} buttonId - The ID to assign to the button.
    * @returns The button element.
    */

    function createButtonElement(buttonText, buttonId) {
        const button = document.createElement("button");
        button.classList.add("guess");
        button.setAttribute("id", buttonId);
        button.setAttribute("type", "button");
        button.textContent = buttonText;
        return button;
    }

    /**
     * @function createMessageElement
     * @description Creates a message element that displays the game status to the player.
     * @param {string} messageText - The message text tells the user how many tiles they have selected and how many tiles are their target
     * @returns The message element.
     */

    function createMessageElement(messageText) {
        const message = document.createElement("span");
        message.classList.add("guess");
        message.textContent = messageText;
        return message;
    }

    /**
    *
    * @function boardColorChange
    * @description depending on its difficulty, the rgb value changes
    *              The higher the difficulty, the smaller the difference between the rgb values,
    *              which is done by the Math.random() function
    * @param {string} difficulty 
    * @returns the rgb value of the tiles on the game board 
    * 
    */
    function boardColorChange(difficulty) {
        function randNum(num) {
            return Math.floor(Math.random() * num);
        }

        if (difficulty === '0') {
            return `rgb(${randNum(225)},${randNum(225)},${randNum(255)})`;
        }
        if (difficulty === '1') {
            let initialVal = randNum(255 - 80);
            return `rgb(${initialVal + randNum(80)},${initialVal + randNum(80)},${initialVal + randNum(80)})`;
        }
        if (difficulty === '2') {
            let initialVal = randNum(255 - 40);
            return `rgb(${initialVal + randNum(40)},${initialVal + randNum(40)},${initialVal + randNum(40)})`;
        }
        if (difficulty === '3') {
            let initialVal = randNum(255 - 10);
            return `rgb(${initialVal + randNum(10)},${initialVal + randNum(10)},${initialVal + randNum(10)})`;
        }
    }


});
