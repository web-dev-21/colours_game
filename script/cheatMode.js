"use strict";

/**
 * @author Ivana Zhekova & Uyen Dinh Michelle Banh
 * 
 * cheatMode.js will allow us to see which is the predominant color on the tiles
 */


document.addEventListener('DOMContentLoaded', function () {
    let cheatMode = false;

    /**
     * Initialize the cheat mode when Shift + C is pressed.
     * @function
     * @description  This function sets up an event listener to detect when Shift + C is pressed, 
     *               and it calls the toggleCheatMode function to enable or disable cheat mode.
     */

    document.addEventListener('keydown', function (event) {
        if (event.shiftKey && event.key === 'C') {
            toggleCheatMode();
        }
    });

    /** 
     * @function toggleCheatMode
     * @description This function calls updateCellsDisplay function to
     *              toggles the cheat mode in order to to show or 
     *              hide color information in board cells, depending on if cheat mode in ON or not 
     */

    function toggleCheatMode() {
        cheatMode = !cheatMode;
        updateCellsDisplay();
    }

    function updateCellsDisplay() {
        const cells = document.querySelectorAll('.gameTable td');

        cells.forEach(function (cell) {
            if (cheatMode) {
                showCheatInfo(cell);
            } else {
                clearCellText(cell);
            }
        });
    }
    /** 
     * @function showCheatInfo
     * @description shows color information in board cells. 
     */

     function showCheatInfo(cell) {
        const rgb = cell.style.backgroundColor;
        const colorName = getDominantColorName(rgb);
        cell.textContent = `${rgb}\n${colorName}`;
        cell.classList.add('cheat');
    }

    /** 
     * @function clearCellText
     * @description hides color information in board cells. 
     */

     function clearCellText(cell) {
        cell.textContent = '';
    }

});

/**
 * @function getRGBValues
 * @description Get the RGB values from an RGB string.
 * @param {string} rgb - The RGB string (e.g., "rgb(255, 0, 0)").
 * @returns {Object} An object containing the individual RGB values.
 */

function getRGBValues(rgb) {
    const matches = rgb.match(/\d+/g);
    if (matches && matches.length === 3) {
        const [r, g, b] = matches.map(Number);
        return { r, g, b };
    }
}


/**
 * @function getDominantColorName
 * @description Get the color name based on RGB values.
 *              The function first determines the dominant
 *              color among the RGB values and handles ties when necessary. 
 * @param {string} rgb - The RGB string in the format "rgb(r, g, b)".
 * @returns {string} The color name ("red," "green," "blue," or "unknown") 
 *                   based on the dominant or tied RGB values, and user's choice.
 */
function getDominantColorName(rgb) {
    const rgbValues = getRGBValues(rgb);
    const userSelectedColor = document.getElementById("color").value;

    if (!rgbValues) {
        return 'unknown';
    }

    const { r, g, b } = rgbValues;

    // checks which color is dominant
    if (r > g && r > b) {
        return 'red';
    } else if (g > r && g > b) {
        return 'green';
    } else if (b > r && b > g) {
        return 'blue';
    }

    // handles tie
    if (r === g && g === b) {
        return userSelectedColor;
    }
    //handles tie involving user-selected color
    if (r === g && (userSelectedColor === 'red' || userSelectedColor === 'green')) {
        return userSelectedColor;
    } else if (r === b && (userSelectedColor === 'red' || userSelectedColor === 'blue')) {
        return userSelectedColor;
    } else if (g === b && (userSelectedColor === 'green' || userSelectedColor === 'blue')) {
        return userSelectedColor;
    }

    //handles tie not involving user-selected color 
    if (r === g) {
        return 'red';
    } else if (r === b) {
        return 'red';
    } else if (g === b) {
        return 'green';
    }

}

