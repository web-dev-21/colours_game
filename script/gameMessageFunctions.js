"use strict";

/**
 * @author Ivana Zhekova & Uyen Dinh Michelle Banh
 * 
 * gameMessageFunctions.js is used to get information about the gameplay.
 * 
 * it gives us the number of selected tiles that the player has clicked;
 * it gives us the number of tiles that the player needs to select in order to get 100%;
 * it gives us the number of correct tiles that the player has clicked.
 * 
 * it sends the completetion percentage of the player's gameplay -> so knowing how well they did or not.
 * 
 */

    /**
    * @description Counts and returns the number of selected tiles
    *              on the game board, as well as updates 
    *              the message for the user as to how many tiles need
    *              to be selected and have been selected
    * 
    * @returns {number} The number of selected tiles.
    */

    function getSelectedTiles() {
        let counter = 0;
        const userColor = document.getElementById("color").value;

        const tds = document.querySelectorAll("td")
        tds.forEach((td) => {
            if (td.classList.contains("selected")) {
                counter++;
            }
        });

        const message = document.getElementsByTagName("span")[0];
        const target = getTargetNumber();

        message.textContent = `Searching for ${userColor}! Your target is ${target}! You have selected ${counter}!`;
        return counter;
    }

    /**
     * @description this function will show a message whenever the submit button will be clicked
     *              The message changes depending on how well or how bad the user is doing
     *              
     * @example for the first player: it will show their score if it is above 0
     *          for players 2-10: it will determine whether the score will show 
     *              -> percentage is above 50%: it will show a success message and show on the leaderboard
     *              -> percentage is below 50%: it will not show on the leaderboard
     */

    function completionPercentage(){

        const message = document.getElementsByTagName("span")[0];

        const nameInput = document.getElementById("name").value;
        const size = document.getElementById('size').value;
        const difficulty = document.getElementById('difficulty').value;
        
        
        let correctlySelected = getCorrectTiles();
        let numSelected = getSelectedTiles();
        let percentage = getPercentage();

        const addScore = getScore(correctlySelected, numSelected, size, difficulty);
        const addPlayer = nameInput;
        
        let highscoreNumbers = document.querySelectorAll("#highscoreBody tr");
        
        if(highscoreNumbers.length === 0 &&  addScore >= 0){
            message.textContent = `${addPlayer} is the first to have a score, will you be able to beat it? Re-enter your information in the form for first place!`; 

           storeLocalStorage(addPlayer, addScore);
            
        }else if( highscoreNumbers.length >= 1 && (percentage >= 50 && addScore >= 0)){
            message.textContent = `${addPlayer.toUpperCase()} IS SO GOOD! You got ${percentage}%! Please re-enter your information in the form to keep playing`;
            
            storeLocalStorage(addPlayer, addScore);

        }else{
            message.textContent = `${addPlayer} failed.. You got ${percentage}%. Please re-enter your information to try again`;
            alert("Your score isn't high enough, you won't find it on the highscore board");
        }
        
    }

    /**
     * @param {string} player 
     * @param {number} score
     * 
     * @description this function will add the player name and score to the local storage
     *              afterwards, it will be appended onto the highscore table
     */

    function storeLocalStorage(player, score){
        const tableBody = document.getElementById("highscoreBody");

        addLocalStorage(player, score);

        while (tableBody.firstChild) {
            tableBody.removeChild(tableBody.firstChild);
        }

        setLocalStorage();  
    }

    /**
     * @description this function will calculate the success rate of the player's gameplay
     * 
     * @example if the player selects the same amount as the target, then it will calculate the percentage
     *          if the player selects 0 correct tiles, they will have 0%
     *          if the player tries to cheat and select every tile, there will be a 15% deduction for each wrong selected tile.             
     * 
     * @returns {number} percentage
     */


    function getPercentage(){
        let percentage;
        let correctlySelected = getCorrectTiles();
        let numSelected = getSelectedTiles();
        let target = getTargetNumber();

        //if the user selects more than the target: deduct them by 15% >:C
        let deductPercentage = (numSelected - correctlySelected)*15;

        if(target === numSelected){
            percentage = parseInt((correctlySelected/target)*100);
        }else if(correctlySelected === 0){
            percentage = 0;
        }
        else{
            percentage = parseInt((correctlySelected/target)*100) - deductPercentage;
        }

        return percentage;
    }


    /**
    * @description Calculates the target number of tiles of the selected color.
    * 
    * @returns {number} The target number of tiles.
    */

    function getTargetNumber() {
        let colorAmount = 0;
        const userColor = document.getElementById("color").value;

        Array.from(document.querySelectorAll(".gameTable td")).forEach((td) => {
            let returnedValue = getDominantColorName(td.style.backgroundColor);

            if (returnedValue === userColor) {
                colorAmount++;
        
            }
        });

        return colorAmount;
    }

    /**
    * @description Counts the number of correct tiles 
    *              that match the chosen color.
    * 
    * @returns {number} The count of correctly selected tiles.
    */

    function getCorrectTiles() {
        let counter = 0;
        const userColor = document.getElementById("color").value;

        Array.from(document.querySelectorAll(".gameTable td"))
            .filter((td) => td.classList.contains("selected"))
            .forEach((td) => {

                if (getDominantColorName(td.style.backgroundColor) === userColor) {
                    counter++;
                }
            });

        return counter;
    }