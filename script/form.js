"use strict";

/**
 * @author Ivana Zhekova & Uyen Dinh Michelle Banh
 * 
 * form.js handles the logic of the user input
 * 
 *  it handles the validation of the player name
 *  it resets the form when the "Submit Guess" button has been clicked
 * 
 */

document.addEventListener("DOMContentLoaded", function (e) {
    const form = document.querySelector("#setup form");
    const nameInput = document.getElementById('name');
    const startButton = document.getElementById("gameStart");
    const userColor = document.getElementById("color");

    nameInput.addEventListener('input', validatePlayerName);
    form.addEventListener("input", toggleStartButton);
    userColor.addEventListener("input", changeStartButtonColor);

    /**
     * @function validatePlayerName
     * @description Ensures name contains only alphanumeric 
     *              characters and is at least 5 characters long,
     *              and if it isn't, it notifies the user.
     */

    function validatePlayerName() {
        const namePattern = /^[a-zA-Z0-9 ]{5,}$/;
        if (namePattern.test(nameInput.value)) {
            nameInput.setCustomValidity("");
        } else {
            nameInput.setCustomValidity("Name must be alphanumeric and at least 5 characters long");
            nameInput.reportValidity();
        }
    }

    /** 
      * @function toggleStartButton
      * @description Enables or disables the "Start Game!" 
      *              button based on the form's validity.
      */

    function toggleStartButton() {
        if (form.checkValidity()) {
            startButton.disabled = false;
        } else {
            startButton.disabled = true;
        }
    }


    /**
     * @function changeStartButtonColor
     * @description changes the "Start Game!" button's background color 
     *              to the selected color
     * @example if user selects the color is red, the button will have a background color of red
     *          if it's blue, it will change to blue, etc.
    */

    function changeStartButtonColor() {
        startButton.style.backgroundColor = userColor.value;
        startButton.style.color = "white";
    }

});

/**
* @function enableFormElements
* @description Enables all form elements (input fields) except 
*              the "Start Game!" button.
*/

function enableFormElements(triggerButton) {
    const form = document.querySelector("#setup form");
    const startButton = document.getElementById("gameStart");
    const formElements = form.elements;

    for (let i = 0; i < formElements.length; i++) {
        formElements[i].disabled = false;
    }
    startButton.disabled = true;
    triggerButton.disabled = true;
}

/**
 * @function disableFormElements
 * Disables all form elements in the provided form.
 * @param  form - The form whose elements will be disabled
 */
function disableFormElements(form) {
    const formElements = form.elements;
    for (let i = 0; i < formElements.length; i++) {
        formElements[i].disabled = true;
    }

}


/**
 * @function resetForm
 * @description Resets the form to its initial state.
 */

function resetForm() {
    const form = document.querySelector("#setup form");
    form.reset();

    const startButton = document.getElementById("gameStart");
    startButton.disabled = false;

    startButton.style.backgroundColor = "";
    startButton.style.color = "";

    enableFormElements(startButton);
}