"use strict";

/**
 * @author Ivana Zhekova & Uyen Dinh Michelle Banh
 * 
 * highscore.js handles the logic behind the highscore table.
 * 
 * When clicking on the highscore header, it will switch its order to know:
 *      who has the highest score
 *      who has the lowest score.
 * 
 * It handles all of the local storage components of our highscore:
 *      it will store the information of our player onto local storage
 *      and then append it to our highscore table.
 */


document.addEventListener("DOMContentLoaded", function (e) {
  let order = false;

    /**
    * @description this event listener will switch the order of the highscore
    *              when clicking on the highscore header:
    *                   it will switch it to descending order or ascending order
    * 
    */

    document.getElementsByTagName("h3")[0].addEventListener("click", function(e){        
        if(order){
            descendingOrder();
            setLocalStorage();
        }else{
            ascendingOrder();
            setLocalStorage();
        }
    });

    /**
     * @description this function will make the highscore in ascending order -> to know the lowest score
     *              after it has changed to ascending order, the order is set to true
     *              this will allow us to make it into descending order again.
     */

    function ascendingOrder(){
      const scores = document.getElementById("highscoreBody");
      scores.textContent = undefined;

      let highscores = [];

      if(localStorage.getItem("highscore")){
          highscores = JSON.parse(localStorage.getItem("highscore"));
      }else 
          return;

      const ascArray = highscores.toSorted( (a,b) => a.points - b.points);
      localStorage.setItem("highscore", JSON.stringify(ascArray));
      order = true;
  }

    /**
    * @description this function will make the highscore in descending order -> to know the highest score
    *              after it has changed to descending order, the order is set to false
    *              this will allow us to make it into ascending order again.
    */

    function descendingOrder(){
        const scores = document.getElementById("highscoreBody");
        scores.textContent = undefined;

        let highscores = [];

        if(localStorage.getItem("highscore")){
            highscores = JSON.parse(localStorage.getItem("highscore"));
        }else 
            return;

        const descArray = highscores.toSorted( (a,b) => b.points - a.points);
        localStorage.setItem("highscore", JSON.stringify(descArray));
        order = false;
    }
});

    /**
    * @description Stores the high scores and player names in the browser's local storage.
    *              Creates the table data to append onto the high score table 
    */

    function setLocalStorage() {
        let highscores = [];

        if(localStorage.getItem("highscore")){
            highscores = JSON.parse(localStorage.getItem("highscore"));
        } else
            return;

        const tableBody = document.getElementById("highscoreBody");

        highscores.forEach((value) => {
            const newRow = document.createElement("tr");
            const playerNames = document.createElement("td");
            playerNames.textContent = value.player;
            newRow.appendChild(playerNames);

            const playerScore = document.createElement("td");
            playerScore.textContent = value.points;
            newRow.appendChild(playerScore);

            tableBody.appendChild(newRow);
        });

        const clearButton = document.getElementById("clear");

         // In your clearButton event listener
        clearButton.addEventListener("click", () => {
            localStorage.clear();
            // Instead of clearing the text content, remove all the child rows from the table body
            const highscoreBody = document.getElementById("highscoreBody");
            while (highscoreBody.firstChild) {
                highscoreBody.removeChild(highscoreBody.firstChild);
            }
        });
    }

    /**
     * @description Adds a player's name and score to the local storage.
     *              When adding the user info onto the highscore, it will be automatically in descending order
     *              This will allow the player to see their top score immediately.
     * 
     * @param {string} name - The name of the player.
     * @param {number} score - The score of the player.
     */

    function addLocalStorage(name, score) {
        let highscores = [];

        if (localStorage.getItem("highscore")) {
            highscores = JSON.parse(localStorage.getItem("highscore"));
        }

        let sortedArray = highscores.toSorted( (a,b) => b.points - a.points);
        
        if(sortedArray.length < 10){
            sortedArray.push({player: name, points: score});
        }

        if(sortedArray.length === 10){
            sortedArray.pop();
            sortedArray.push({player: name, points: score});
        }

        sortedArray.sort((a, b) => b.points - a.points);

        localStorage.setItem("highscore", JSON.stringify(sortedArray));
    }

    /**
     * @author Samuel Imberman
     * 
     * @description Calculates the score based on the number of correctly guessed colors, the number of selected tiles, the size of the board, and the difficulty.
     * @param {number} numCorrect - Number of correctly guessed colors.
     * @param {number} numSelected - Number of selected tiles.
     * @param {number} boardSize - Size of the game board.
     * @param {number} difficulty - The difficulty level of the game.
     * 
     * @returns {number} The calculated score.
     */

    function getScore(numCorrect, numSelected, boardSize, difficulty) {
        const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
        return Math.floor(percent * 100 * boardSize * (difficulty + 1));
    }

